=============
FunkLoad demo
=============
$Id: README.txt 53544 2009-03-09 16:28:58Z tlazar $

You can find 4 examples of FunkLoad usage here:

* simple/
  An example on how to use test and bench runner with monitoring.

* zope/
  A basic ZopeTestCase example.

* cmf/
  An test case that requires a credential server.

* xmlrpc/
  An example on how to test/bench an XML RPC server with a Makefile example.
